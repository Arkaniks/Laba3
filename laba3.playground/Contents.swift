
public protocol KeyValuePair : Comparable {
    associatedtype K : Comparable, Hashable
    associatedtype V : Comparable, Hashable

    var key : K {get set}
    var value : V {get set}

    init(key: K, value: V)

    func copy() -> Self
}

extension KeyValuePair {
    public static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key
    }

    public static func <(lhs: Self, rhs: Self) -> Bool {
        return lhs.key < rhs.key
    }

    public static func <=(lhs: Self, rhs: Self) -> Bool {
        return lhs.key <= rhs.key
    }

    public static func >=(lhs: Self, rhs: Self) -> Bool {
        return lhs.key >= rhs.key
    }

    public static func >(lhs: Self, rhs: Self) -> Bool {
        return lhs.key > rhs.key
    }
}

struct IntegerPair : KeyValuePair {
    var key : Int
    var value : Int

    func copy() -> IntegerPair {
        return IntegerPair(key: self.key, value: self.value)
    }
}

protocol Queue {
    associatedtype Item
    mutating func enqueue(item: Item) throws
    func getFirst() -> Item?
    mutating func dequeue() -> Item?
}

fileprivate class IndirectStorage<T> {
    var head: SinglyLinkedListNode<T>?
    var tail: SinglyLinkedListNode<T>?
    init(head: SinglyLinkedListNode<T>?, tail: SinglyLinkedListNode<T>?) {
        self.head = head
        self.tail = tail
    }

    convenience init() {
        self.init(head: nil, tail: nil)
    }
}

public class SinglyLinkedListNode<T> {
    public var value: T
    public var next: SinglyLinkedListNode<T>?
    public init(value: T) {
        self.value = value
    }
}

public struct SinglyLinkedList<T> {
    private var storage: IndirectStorage<T>
    private var storageForWritting: IndirectStorage<T> {
        mutating get {
            if !isKnownUniquelyReferenced(&self.storage) {
                self.storage = self.copyStorage()
            }

            return self.storage
        }
    }

    public var last: T? {
        get {
            return self.storage.tail?.value
        }
    }

    init(head: SinglyLinkedListNode<T>) {
        self.storage = IndirectStorage()
        self.append(node: head)
    }

    public init(value: T) {
        let node = SinglyLinkedListNode<T>(value: value)
        self.init(head: node)
    }

    public init() {
        self.storage = IndirectStorage()
    }

    public mutating func append(value: T) {
        let node = SinglyLinkedListNode<T>(value: value)
        self.append(node: node)
    }

    public mutating func prepend(value: T) {
        let node = SinglyLinkedListNode<T>(value: value)
        self.prepend(node: node)
    }

    public mutating func deleteItem(at index:Int) -> T {
        precondition((index >= 0) && (index < self.count))
        var previous: SinglyLinkedListNode<T>? = nil
        var current = self.storageForWritting.head
        var i = 0
        var elementToDelete: SinglyLinkedListNode<T>
        while (i < index) {
            previous = current
            current = current?.next
            i += 1
        }

        elementToDelete = current!
        if (self.storage.head === current) {
            self.storageForWritting.head = current?.next
        }

        if (self.storage.tail === current) {
            self.storageForWritting.tail = previous
        }

        previous?.next = current?.next
        return elementToDelete.value
    }

    private func find(kthToLast: UInt, startingAt node: SinglyLinkedListNode<T>?, count: UInt) -> SinglyLinkedListNode<T>? {
        guard kthToLast <= count else {
            return nil
        }

        guard (node != nil) else {
            return nil
        }

        let i = (count - kthToLast)
        if (i == 0) {
            return node
        }

        return find(kthToLast: kthToLast, startingAt: node?.next, count: (count - 1))
    }

    public func find(kthToLast: UInt) -> SinglyLinkedListNode<T>? {
        find(kthToLast: kthToLast, startingAt: self.storage.head, count: UInt(self.count))
    }

    public func containsLoop() -> Bool {
        var current = self.storage.head
        var runner = self.storage.head
        while (runner != nil) && (runner?.next != nil) {
            current = current?.next
            runner = runner?.next?.next
            if runner === current {
                return true
            }
        }

        return false
    }

    public mutating func printAll() {
       var element = storageForWritting.head
        while element?.next != nil {
            print(element?.value)
            element = element?.next
        }

        print(element?.value)
    }

    public func popLast() {

    }
}

private extension SinglyLinkedList {
    private mutating func prepend(node: SinglyLinkedListNode<T>) {
        let (tailFromNewNode, _) = findTail(in: node)
        tailFromNewNode.next = self.storageForWritting.head
        self.storageForWritting.head = node

        if self.storage.tail == nil {
            self.storageForWritting.tail = tailFromNewNode
        }
    }

    private mutating func append(node: SinglyLinkedListNode<T>) {
        if self.storage.tail != nil {
            self.storageForWritting.tail?.next = node
            if !self.containsLoop() {
                let (tail, _) = findTail(in: node)
                self.storageForWritting.tail = tail // There
            } else {
                self.storageForWritting.tail = nil
            }
        } else {
            self.storageForWritting.head = node
            if !self.containsLoop() {
                let (tail, _) = findTail(in: node)
                self.storageForWritting.tail = tail // There
            } else {
                self.storageForWritting.tail = nil
            }
        }
    }

    private func copyStorage() -> IndirectStorage<T> {
        guard (self.storage.head != nil) && (self.storage.tail != nil) else {
            return IndirectStorage(head: nil, tail: nil)
        }

        let copiedHead = SinglyLinkedListNode<T>(value: self.storage.head!.value)
        var previousCopied: SinglyLinkedListNode<T> = copiedHead
        var current: SinglyLinkedListNode<T>? = self.storage.head?.next
        while (current != nil) {
            let currentCopy = SinglyLinkedListNode<T>(value: current!.value)
            previousCopied.next = currentCopy
            current = current?.next
            previousCopied = currentCopy
        }

        return IndirectStorage(head: copiedHead, tail: previousCopied)
    }
}

extension SinglyLinkedList where T: Comparable {
    public mutating func deleteNode(withValue v: T) {
        guard self.storage.head != nil else {
            return
        }

        var previous: SinglyLinkedListNode<T>? = nil
        var current = self.storage.head
        while (current != nil) && (current?.value != v) {
            previous = current
            current = current?.next
        }

        if let foundNode = current {
            if (self.storage.head === foundNode) {
                self.storageForWritting.head = foundNode.next
            }

            if (self.storage.tail === foundNode) {
                self.storage.tail = previous
            }

            previous?.next = foundNode.next
            foundNode.next = nil
        }
    }

    ///  O(N^2)
    public mutating func deleteDuplicatesInPlace() {
        var current = self.storageForWritting.head
        while (current != nil) {
            var previous: SinglyLinkedListNode<T>? = current
            var next = current?.next
            while (next != nil) {
                if (current?.value == next?.value) {
                    if (self.storage.head === next) {
                        self.storage.head = next?.next
                    }

                    if (self.storage.tail === next) {
                        self.storage.tail = previous
                    }

                    // Delete next
                    previous?.next = next?.next
                }

                previous = next
                next = next?.next
            }

            current = current?.next
        }
    }
}

public struct SinglyLinkedListForwardIterator<T> : IteratorProtocol {
    public typealias Element = T
    private(set) var head: SinglyLinkedListNode<T>?

    mutating public func next() -> T? {
        let result = self.head?.value
        self.head = self.head?.next
        return result
    }
}

extension SinglyLinkedList : Sequence {
    public func makeIterator() -> SinglyLinkedListForwardIterator<T> {
        return SinglyLinkedListForwardIterator(head: self.storage.head)
    }
}

extension SinglyLinkedList : Collection {
    public typealias Index = SinglyLinkedListIndex<T>
    public var startIndex: Index {
        get {
            SinglyLinkedListIndex<T>(node: self.storage.head, tag: 0)
        }
    }

    public var endIndex: Index {
        get {
            if let h = self.storage.head {
                let (_, numberOfElements) = findTail(in: h)
                return SinglyLinkedListIndex<T>(node: h, tag: numberOfElements)
            } else {
                return SinglyLinkedListIndex<T>(node: nil, tag: self.startIndex.tag)
            }
        }
    }

    public subscript(position: Index) -> T {
        get {
            return position.node!.value
        }
    }

    public func index(after idx: Index) -> Index {
        return SinglyLinkedListIndex<T>(node: idx.node?.next, tag: idx.tag+1)
    }
}

extension SinglyLinkedList : Queue {
    typealias Item = T

    func getFirst() -> T? {
        return self.storage.head?.value
    }

    mutating func enqueue(item: T) throws {
        self.append(node: SinglyLinkedListNode<T>(value: item))
    }

    mutating func dequeue() -> T? {
        guard self.count > 0 else {
            return nil
        }

        return self.deleteItem(at: 0)
    }
}

extension SinglyLinkedList : ExpressibleByArrayLiteral {
    public typealias Element = T

    public init(arrayLiteral elements: Element...) {
        var headSet = false
        var current : SinglyLinkedListNode<T>?
        var numberOfElements = 0
        self.storage = IndirectStorage()

        for element in elements {
            numberOfElements += 1

            if headSet == false {
                self.storage.head = SinglyLinkedListNode<T>(value: element)
                current = self.storage.head
                headSet = true
            } else {
                let newNode = SinglyLinkedListNode<T>(value: element)
                current?.next = newNode
                current = newNode
            }
        }

        self.storage.tail = current
    }
}

public struct SinglyLinkedListIndex<T> : Comparable {
    fileprivate let node: SinglyLinkedListNode<T>?
    fileprivate let tag: Int

    public static func==<T>(lhs: SinglyLinkedListIndex<T>, rhs: SinglyLinkedListIndex<T>) -> Bool {
        return (lhs.tag == rhs.tag)
    }

    public static func< <T>(lhs: SinglyLinkedListIndex<T>, rhs: SinglyLinkedListIndex<T>) -> Bool {
        return (lhs.tag < rhs.tag)
    }
}

extension SinglyLinkedList where T : KeyValuePair {

    public func find(elementWithKey key: T.K) -> T? {
        let searchResults = self.filter { (keyValuePair) -> Bool in
            return keyValuePair.key == key
        }

        return searchResults.first
    }
}

func findTail<T>(in node: SinglyLinkedListNode<T>) -> (tail: SinglyLinkedListNode<T>, count: Int) {
    var current: SinglyLinkedListNode<T>? = node
    var count = 1

    while (current?.next != nil) {
        current = current?.next
        count += 1
    }

    if current != nil {
        return (tail: current!, count: count)
    } else {
        return (tail: node, count: 1)
    }
}

var l1: SinglyLinkedList<Int> = [1,2,3,4,5,6,7]
var l2 = l1
l2.append(value: 67)
assert(l1.count == 7)
assert(l1.contains(67) == false)
assert(l2.count == 8)
assert(l2.contains(67) == true)
let l3 = l2.dropLast()
l3.isEmpty
assert(l3.count == 7)
assert(l3.contains(67) == false)

public struct Stack<T> {
    fileprivate var singlyLinkedList: SinglyLinkedList<T>
  public var isEmpty: Bool {
    return singlyLinkedList.isEmpty
  }
  public var count: Int {
    return singlyLinkedList.count
  }

  public mutating func push(_ element: T) {
    singlyLinkedList.append(value: element)
  }

  public mutating func pop() -> T? {
    singlyLinkedList.deleteItem(at: 0)
  }

  public var top: T? {
    singlyLinkedList.last
  }

    public mutating func printAll() {
        singlyLinkedList.printAll()
    }
}

extension Stack: Sequence {
  public func makeIterator() -> AnyIterator<T> {
    var curr = self
    return AnyIterator { curr.pop() }
  }
}

var stackOfNames = Stack<String>(singlyLinkedList: ["Carl", "Lisa", "Stephanie", "Jeff", "Wade"])
stackOfNames.push("Mike")
print(stackOfNames.count)
print(stackOfNames.singlyLinkedList)
stackOfNames.printAll()

//stackOfNames.pop()
stackOfNames.top
stackOfNames.isEmpty


internal enum OperatorAssociativity {
  case leftAssociative
  case rightAssociative
}

public enum OperatorType: CustomStringConvertible {
  case add
  case subtract
  case divide
  case multiply
  case percent
  case exponent

  public var description: String {
    switch self {
    case .add:
      return "+"
    case .subtract:
      return "-"
    case .divide:
      return "/"
    case .multiply:
      return "*"
    case .percent:
      return "%"
    case .exponent:
      return "^"
    }
  }
}

public enum TokenType: CustomStringConvertible {
  case openBracket
  case closeBracket
  case Operator(OperatorToken)
  case operand(Double)

  public var description: String {
    switch self {
    case .openBracket:
      return "("
    case .closeBracket:
      return ")"
    case .Operator(let operatorToken):
      return operatorToken.description
    case .operand(let value):
      return "\(value)"
    }
  }
}

public struct OperatorToken: CustomStringConvertible {
  let operatorType: OperatorType

  init(operatorType: OperatorType) {
    self.operatorType = operatorType
  }

  var precedence: Int {
    switch operatorType {
    case .add, .subtract:
      return 0
    case .divide, .multiply, .percent:
      return 5
    case .exponent:
      return 10
    }
  }

  var associativity: OperatorAssociativity {
    switch operatorType {
    case .add, .subtract, .divide, .multiply, .percent:
      return .leftAssociative
    case .exponent:
      return .rightAssociative
    }
  }

  public var description: String {
    return operatorType.description
  }
}

func <= (left: OperatorToken, right: OperatorToken) -> Bool {
  return left.precedence <= right.precedence
}

func < (left: OperatorToken, right: OperatorToken) -> Bool {
  return left.precedence < right.precedence
}

public struct Token: CustomStringConvertible {
  let tokenType: TokenType

  init(tokenType: TokenType) {
    self.tokenType = tokenType
  }

  init(operand: Double) {
    tokenType = .operand(operand)
  }

  init(operatorType: OperatorType) {
    tokenType = .Operator(OperatorToken(operatorType: operatorType))
  }

  var isOpenBracket: Bool {
    switch tokenType {
    case .openBracket:
      return true
    default:
      return false
    }
  }

  var isOperator: Bool {
    switch tokenType {
    case .Operator(_):
      return true
    default:
      return false
    }
  }

  var operatorToken: OperatorToken? {
    switch tokenType {
    case .Operator(let operatorToken):
      return operatorToken
    default:
      return nil
    }
  }

  public var description: String {
    return tokenType.description
  }
}

public class InfixExpressionBuilder {
  private var expression = [Token]()

  public func addOperator(_ operatorType: OperatorType) -> InfixExpressionBuilder {
    expression.append(Token(operatorType: operatorType))
    return self
  }

  public func addOperand(_ operand: Double) -> InfixExpressionBuilder {
    expression.append(Token(operand: operand))
    return self
  }

  public func addOpenBracket() -> InfixExpressionBuilder {
    expression.append(Token(tokenType: .openBracket))
    return self
  }

  public func addCloseBracket() -> InfixExpressionBuilder {
    expression.append(Token(tokenType: .closeBracket))
    return self
  }

  public func build() -> [Token] {
    // Maybe do some validation here
    return expression
  }
}

// This returns the result of the shunting yard algorithm
public func reversePolishNotation(_ expression: [Token]) -> String {

    var tokenStack = Stack<Token>(singlyLinkedList: <#SinglyLinkedList<Token>#>)
  var reversePolishNotation = [Token]()

  for token in expression {
    switch token.tokenType {
    case .operand(_):
      reversePolishNotation.append(token)

    case .openBracket:
      tokenStack.push(token)

    case .closeBracket:
      while tokenStack.count > 0, let tempToken = tokenStack.pop(), !tempToken.isOpenBracket {
        reversePolishNotation.append(tempToken)
      }

    case .Operator(let operatorToken):
      for tempToken in tokenStack.makeIterator() {
        if !tempToken.isOperator {
          break
        }

        if let tempOperatorToken = tempToken.operatorToken {
          if operatorToken.associativity == .leftAssociative && operatorToken <= tempOperatorToken
            || operatorToken.associativity == .rightAssociative && operatorToken < tempOperatorToken {
            reversePolishNotation.append(tokenStack.pop()!)
          } else {
            break
          }
        }
      }
      tokenStack.push(token)
    }
  }

  while tokenStack.count > 0 {
    reversePolishNotation.append(tokenStack.pop()!)
  }

  return reversePolishNotation.map({token in token.description}).joined(separator: " ")
}

// Simple demo
let expr = InfixExpressionBuilder()
  .addOperand(3)
  .addOperator(.add)
  .addOperand(4)
  .addOperator(.multiply)
  .addOperand(2)
  .addOperator(.divide)
  .addOpenBracket()
  .addOperand(1)
  .addOperator(.subtract)
  .addOperand(5)
  .addCloseBracket()
  .addOperator(.exponent)
  .addOperand(2)
  .addOperator(.exponent)
  .addOperand(3)
  .build()

print(expr.description)
print(reversePolishNotation(expr))
